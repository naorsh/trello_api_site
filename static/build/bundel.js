/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _authentication = __webpack_require__(1);

	var _authentication2 = _interopRequireDefault(_authentication);

	var _boards = __webpack_require__(2);

	var _boards2 = _interopRequireDefault(_boards);

	var _cards = __webpack_require__(3);

	var _cards2 = _interopRequireDefault(_cards);

	var _add_card = __webpack_require__(4);

	var _add_card2 = _interopRequireDefault(_add_card);

	var _filters = __webpack_require__(5);

	var _filters2 = _interopRequireDefault(_filters);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	_boards2.default;
	//loadCards;

	//import filter_cards from './filters';
	_add_card2.default;
	_filters2.default;

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _boards = __webpack_require__(2);

	var auth = function auth() {
	    Trello.authorize({
	        type: "popup",
	        name: "Trello dashboard",
	        scope: {
	            read: true,
	            write: true
	        },
	        expiration: "never",
	        success: _boards.loadBoards,
	        error: function error() {
	            console.log("Failed authentication");
	        }
	    });
	};

	exports.default = auth();

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.loadedBoards = loadedBoards;
	exports.loadBoards = loadBoards;
	exports.loadedLists = loadedLists;

	var _cards = __webpack_require__(3);

	function loadedBoards(boards) {
	    $.each(boards, function (index, value) {
	        $('#boards').append($("<option></option>").attr("value", value.id).text(value.name));
	    });
	    Trello.get('/boards/' + boards[0].id + '/lists/', loadedLists, function () {
	        console.log("Failed to load list");
	    });
	}

	function loadBoards() {
	    Trello.get('/members/me/boards/', loadedBoards, function () {
	        console.log("Failed to load boards");
	    });
	}

	function loadedLists(lists) {
	    $('#choose_list_option').empty();
	    $.each(lists, function (index, value) {
	        $('#lists').append($('<div id="' + value.id + '" class="list_div"><h4>' + value.name + '</h4><br><div class="list_content"></div></div>'));
	        $('#choose_list_option').append($("<option></option>").attr("value", value.id).text(value.name));
	    });
	    $.each(lists, function (index, value) {
	        Trello.get('/lists/' + value.id + '/cards/', _cards.loadedCards, function () {
	            console.log("Failed to load boards");
	        });
	    });
	}

	var loadLists = function loadLists() {
	    var load = function load() {
	        $('#boards').change(function () {
	            var board_id = $("option:selected", this).val();
	            $('#lists').empty();
	            Trello.get('boards/' + board_id + '/lists', loadedLists, function () {
	                console.log("Failed to load lists");
	            });
	        });
	    };
	    load();
	};
	exports.default = loadLists();

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.insert_card = insert_card;
	exports.add_members_to_card = add_members_to_card;
	exports.loadedCards = loadedCards;
	function insert_card(list_id, card_name, due, card_id) {

	    var list_id_selector = "#" + list_id;
	    $("#" + list_id + " > .list_content").append("<div class='note' id='" + card_id + "'>" + "<div class='card_header_div'><p>" + card_name + "</p></div>" + "</div>");
	    if (due != null) {
	        $("#" + card_id).append("<p>Due: " + due + "</p>");
	    }
	}

	function add_members_to_card(card) {
	    card.idMembers.forEach(function (member_id) {
	        Trello.get('/members/' + member_id, function (member) {
	            $('#' + card.id).append("<p>" + member.fullName + "</p>");
	        }, function () {
	            console.log("failed to get members id");
	        });
	    });
	}

	function loadedCards(cards) {
	    $.each(cards, function (index, card) {
	        insert_card(card.idList, card.name, card.due, card.id);
	        add_members_to_card(card);
	    });
	}

	//export function loadedCards(cards)

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _cards = __webpack_require__(3);

	var add_card = function add_card() {
	    var load = function load() {
	        $('#new_card_submit').click(function (e) {
	            e.preventDefault();
	            console.log('added card');
	            var newCard = { name: $("#new_card_text").val(),
	                desc: "",
	                pos: "bottom",
	                due: null,
	                idList: $("option:selected", "#choose_list_option").val()
	            };
	            $("#new_card_text").val("");
	            Trello.post('/cards/', newCard, function () {
	                console.log("succeeded to add card");
	            }, function () {
	                console.log("Failed to add card");
	            });
	            var list_to_add_card_to = document.getElementById("choose_list_option").value;
	            (0, _cards.insert_card)(list_to_add_card_to, newCard.name);
	        });
	    };
	    load();
	};
	exports.default = add_card();

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _cards = __webpack_require__(3);

	function insert_cards_filtered_by_headline(cards) {
	    var filtered_cards_list = cards.filter(function (cards) {
	        var headline = $('#filter_input').val();
	        return cards.name.toUpperCase() === headline.toUpperCase();
	    });
	    (0, _cards.loadedCards)(filtered_cards_list);
	}

	function insert_cards_filtered_by_member(cards) {
	    cards.forEach(function (card) {
	        card.idMembers.forEach(function (member_id) {
	            Trello.get('/members/' + member_id, function (member) {
	                var member_name = $('#filter_input').val();
	                if (member.fullName.toUpperCase() === member_name.toUpperCase()) {
	                    (0, _cards.insert_card)(card.idList, card.name, card.due, card.id);
	                    (0, _cards.add_members_to_card)(card);
	                }
	            }, function () {
	                console.log("failed to get members id");
	            });
	        });
	    });
	}

	function insert_cards_filtered_by_due_date(cards) {
	    var date_to_filter_by = $('#date_input').val();
	    var filtered_cards_list = cards.filter(function (cards) {
	        return cards.due != null && cards.due.indexOf(date_to_filter_by) > -1;
	    });
	    (0, _cards.loadedCards)(filtered_cards_list);
	}

	var insert_filtered_cards = function insert_filtered_cards(cards) {
	    if ($('#filter_select').val() === "headline") {
	        insert_cards_filtered_by_headline(cards);
	    }
	    if ($('#filter_select').val() === "Member") {
	        insert_cards_filtered_by_member(cards);
	    }
	    if ($('#filter_select').val() === "Due date") {
	        insert_cards_filtered_by_due_date(cards);
	    }
	};

	var filter_cards = function filter_cards() {
	    $('#filter_select').change(function () {
	        if ($('#filter_select').val() === "Due date") {
	            $('#date_input').show();
	            $('#filter_input').hide();
	        } else {
	            $('#date_input').hide();
	            $('#filter_input').show();
	        }
	    });
	    $('#filter_submit').click(function (e) {
	        e.preventDefault();
	        var current_board_id = $("#boards>option:selected").val();
	        $('#lists > div > .list_content > div').remove();
	        Trello.get("/boards/" + current_board_id + "/cards/", insert_filtered_cards, function () {
	            console.log("Failed to filter");
	        });
	    });
	};
	exports.default = filter_cards();

/***/ }
/******/ ]);