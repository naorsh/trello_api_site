import {loadedCards} from './cards';


export function loadedBoards(boards) {
    $.each(boards, function(index, value) {
        $('#boards')
            .append($("<option></option>")
                .attr("value",value.id)
                .text(value.name));
    });
    Trello.get(
              '/boards/' + boards[0].id + '/lists/',
              loadedLists,
              function() { console.log("Failed to load list"); }
          );
}

export function loadBoards() {
	      Trello.get(
              '/members/me/boards/',
              loadedBoards,
              function() { console.log("Failed to load boards"); }
          );
}



export function loadedLists(lists) {
    $('#choose_list_option').empty();
    $.each(lists, function(index, value) {
        $('#lists')
            .append($('<div id="' + value.id +'" class="list_div"><h4>' + value.name + '</h4><br><div class="list_content"></div></div>'));
        $('#choose_list_option')
            .append($("<option></option>")
                .attr("value",value.id)
                .text(value.name));
    });
     $.each(lists, function(index, value) {
         Trello.get(
             '/lists/' + value.id + '/cards/',
             loadedCards,
             function () {
                 console.log("Failed to load boards");
             }
         );
     });
}

const loadLists = function(){
    let load = function (){
        $('#boards').change(function() {
            var board_id = $("option:selected", this).val();
            $('#lists').empty();
            Trello.get(
                'boards/' + board_id + '/lists',
                loadedLists,
                function() { console.log("Failed to load lists"); }
            );
        });
    };
    load();
};
export default loadLists()

