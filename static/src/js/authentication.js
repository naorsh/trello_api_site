import {loadBoards} from './boards'

const auth = function () {
    Trello.authorize({
        type: "popup",
        name: "Trello dashboard",
        scope: {
            read: true,
            write: true
        },
        expiration: "never",
        success: loadBoards,
        error: function () {
            console.log("Failed authentication");
        }
    });
};

export default auth();



