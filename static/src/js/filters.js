import {insert_card} from './cards';
import {loadedCards} from './cards'
import {add_members_to_card} from './cards'

function insert_cards_filtered_by_headline(cards) {
    let filtered_cards_list = cards.filter(function(cards) {
        let headline = $('#filter_input').val();
        return cards.name.toUpperCase() === headline.toUpperCase();
    });
    loadedCards(filtered_cards_list);
}

function insert_cards_filtered_by_member(cards) {
    cards.forEach(function (card) {
            card.idMembers.forEach( function(member_id){
                Trello.get (
                  '/members/' + member_id,
                  function(member) {
                      let member_name = $('#filter_input').val();
                      if (member.fullName.toUpperCase() === member_name.toUpperCase()) {
                          insert_card(card.idList, card.name, card.due, card.id);
                          add_members_to_card(card);
                      }
                  } ,
                  function () {console.log("failed to get members id")}
                      );
            });
        });
}

function insert_cards_filtered_by_due_date(cards) {
    let date_to_filter_by = $('#date_input').val();
    let filtered_cards_list = cards.filter(function (cards) {
        return (cards.due!= null) && (cards.due.indexOf(date_to_filter_by) > -1);
    });
    loadedCards(filtered_cards_list);
}

let  insert_filtered_cards = function(cards) {
    if ($('#filter_select').val() === "headline") {
        insert_cards_filtered_by_headline(cards);
    }
    if ($('#filter_select').val() === "Member") {
        insert_cards_filtered_by_member(cards);
    }
    if ($('#filter_select').val() === "Due date") {
            insert_cards_filtered_by_due_date(cards);
    }
};


const filter_cards = function(){
    $('#filter_select').change(function () {
        if ($('#filter_select').val() === "Due date") {
           $('#date_input').show();
           $('#filter_input').hide();
        }
        else {
            $('#date_input').hide();
           $('#filter_input').show();
        }

    });
    $('#filter_submit').click(function(e) {
        e.preventDefault();
        let current_board_id = $("#boards>option:selected").val();
        $('#lists > div > .list_content > div').remove();
        Trello.get(
            "/boards/" + current_board_id + "/cards/",
            insert_filtered_cards,
            function() { console.log("Failed to filter"); }
        );
    });
};
export default filter_cards()

