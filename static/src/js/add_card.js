import {insert_card} from './cards';

const add_card = function(){
    let load = function (){
        $('#new_card_submit').click(function(e) {
            e.preventDefault();
            console.log('added card');
            var newCard =
              {name: $("#new_card_text").val(),
              desc: "",
              pos: "bottom",
              due: null,
              idList: $("option:selected", "#choose_list_option").val()
              };
            $("#new_card_text").val("");
            Trello.post(
                '/cards/',
                newCard,
                function() { console.log("succeeded to add card"); },
                function() { console.log("Failed to add card"); }
            );
            let list_to_add_card_to = document.getElementById("choose_list_option").value;
            insert_card(list_to_add_card_to, newCard.name)
        });
    };
    load();
};
export default add_card()
