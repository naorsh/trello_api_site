


export function insert_card(list_id, card_name,due, card_id) {

    let list_id_selector = "#" + list_id;
    $("#" + list_id + " > .list_content").append(
        "<div class='note' id='" + card_id + "'>" +
        "<div class='card_header_div'><p>" + card_name + "</p></div>" +
        "</div>");
    if (due != null) {
        $("#" + card_id).append("<p>Due: " + due + "</p>");
    }
}


export function add_members_to_card(card) {
    card.idMembers.forEach(function (member_id) {
        Trello.get(
            '/members/' + member_id,
            function (member) {
                $('#' + card.id).append("<p>" + member.fullName + "</p>");
            },
            function () {
                console.log("failed to get members id")
            }
        );
    });
}

export function loadedCards(cards) {
    $.each(cards, function (index, card) {
        insert_card(card.idList, card.name, card.due, card.id);
        add_members_to_card(card);
    });
}

//export function loadedCards(cards)
